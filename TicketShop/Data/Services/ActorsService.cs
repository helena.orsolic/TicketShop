﻿using Microsoft.EntityFrameworkCore;
using TicketShop.Data.Services;
using TicketShop.Data;
using TicketShop.Models;
using TicketShop.Data.Base;

namespace eTickets.Data.Services
{
    public class ActorsService : EntityBaseRepository<Actor>, IActorsService
    {
        private readonly AppDBContext _context;
        public ActorsService(AppDBContext context) : base(context) { }

    }
}
