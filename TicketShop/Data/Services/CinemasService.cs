﻿using TicketShop.Data.Base;
using TicketShop.Models;

namespace TicketShop.Data.Services
{
    public class CinemasService : EntityBaseRepository<Cinema>, ICinemasService
    {
        public CinemasService(AppDBContext dbContext) : base(dbContext) { }
    }
}
