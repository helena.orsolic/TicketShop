﻿using TicketShop.Data.Base;
using TicketShop.Models;

namespace TicketShop.Data.Services
{
    public interface IActorsService : IEntityBaseRepository<Actor>
    {

    }
}
