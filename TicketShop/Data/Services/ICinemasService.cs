﻿using TicketShop.Data.Base;
using TicketShop.Models;

namespace TicketShop.Data.Services
{
    public interface ICinemasService : IEntityBaseRepository<Cinema>
    {
    }
}
