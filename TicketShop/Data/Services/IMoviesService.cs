﻿using TicketShop.Data.Base;
using TicketShop.Data.ViewModels;
using TicketShop.Models;

namespace TicketShop.Data.Services
{
    public interface IMoviesService : IEntityBaseRepository<Movie>
    {
        Task<Movie> GetMovieByIdAsync(int id);

        Task<NewMovieDropdowns> GetNewMovieDropdownsValues();
        Task AddNewMovieAsync(NewMovieViewModel data);
        Task UpdateMovieAsync(NewMovieViewModel data);
    }
}
