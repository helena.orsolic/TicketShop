﻿using TicketShop.Models;

namespace TicketShop.Data.Services
{
    public interface IOrdersServicecs
    {
        Task StoreOrderAsync(List<ShoppingCartItem> items, string userId, string userEmailAddress);
        Task<List<Order>> GetOrdersByUserIdAndRoleAsync(string userId, string userRole);
    }
}
