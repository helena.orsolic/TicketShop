﻿using TicketShop.Data.Base;
using TicketShop.Models;

namespace TicketShop.Data.Services
{
    public class ProducersService : EntityBaseRepository<Producer>, IProducersService
    {
        public ProducersService(AppDBContext context) : base(context) { }
    }
}
