﻿using System.ComponentModel.DataAnnotations;
using TicketShop.Data.Base;

namespace TicketShop.Models
{
    public class Actor : IEntityBase
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Profile picture")]
        [Required(ErrorMessage = "Profile picture is required")]
        public string ProfilePictureURL { get; set; }

        [Display(Name = "Actor name")]
        [Required(ErrorMessage = "Actor name is required")]
        [StringLength(50, MinimumLength =3, ErrorMessage = "Name must be between 3 and 50 charachters")]
        public string FullName { get; set; }

        [Display(Name = "Actor biography")]
        [Required(ErrorMessage = "Actor biography is required")]
        public string Biography { get; set; }

        //Relationships
        public List<Actor_Movie>? Actors_Movies { get; set; }
    }
}
