﻿using System.ComponentModel.DataAnnotations;
using TicketShop.Data.Base;

namespace TicketShop.Models
{
    public class Cinema : IEntityBase
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Cinema logo")]
        public string Logo { get; set; }

        [Display(Name = "Cinema name")]
        public string Name { get; set; }

        [Display(Name = "Cinema description")]
        public string Description { get; set; }

        //Relationships
        public List<Movie>? Movies { get; set; }
    }
}
