﻿using System.ComponentModel.DataAnnotations;
using TicketShop.Data.Base;

namespace TicketShop.Models
{
    public class Producer : IEntityBase
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Profile picture")]
        [Required(ErrorMessage = "Profile picture is required")]
        public string ProfilePictureURL { get; set; }

        [Display(Name = "Producer name")]
        [Required(ErrorMessage = "Producer name is required")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Name must be between 3 and 50 charachters")]
        public string FullName { get; set; }

        [Display(Name = "Producer biography")]
        [Required(ErrorMessage = "Producer biography is required")]
        public string Biography { get; set; }

        ///Relationships
        public List<Movie>? Movies { get; set; }
    }
}
